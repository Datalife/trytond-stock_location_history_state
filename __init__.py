# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import location
from . import unit_load


def register():
    Pool.register(
        location.Location,
        location.LocationState,
        module='stock_location_history_state', type_='model')
    Pool.register(
        location.Combined,
        module='stock_location_history_state', type_='model',
        depends=['stock_location_combined'])
    Pool.register(
        unit_load.UnitLoad,
        module='stock_location_history_state', type_='model',
        depends=['stock_unit_load'])
    Pool.register(
        unit_load.UnitLoadCombined,
        module='stock_location_history_state', type_='model',
        depends=['stock_unit_load_location_combined'])
