# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta
from sql.aggregate import Max
from sql.conditionals import Coalesce
from sql.functions import RowNumber
from sql import Table, Null, Window
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Or, If
from trytond.transaction import Transaction
from itertools import groupby

STATES = [
    (None, ''),
    ('on', 'On'),
    ('off', 'Off')
]


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    state = fields.Selection(STATES, 'State', readonly=True,
        domain=[If(Eval('type') == 'production',
            ('state', '!=', None), ())],
        states={
            'readonly': ~Eval('active'),
            'invisible': Eval('type') != 'production',
        },
        depends=['active', 'type'])
    state_icon = fields.Function(
        fields.Char('State Icon'), 'get_state_icon')
    state_at = fields.Function(
        fields.Selection(STATES, 'State at'),
        'get_state_at', searcher='search_state_at')
    states = fields.One2Many('stock.location.state', 'location',
        'History State', loading='lazy', states={
            'invisible': Eval('type') != 'production',
        },
        context={'create_history_state': False},
        order=[('date', 'DESC')], depends=['type'])
    last_states = fields.Function(
            fields.One2Many('stock.location.state', 'location',
                'History State', states={
                    'invisible': Eval('type') != 'production',
                },
                context={'create_history_state': False},
                order=[('date', 'DESC')], depends=['type']),
        'get_last_states', setter='set_last_states')
    _last_states_size = 10

    @classmethod
    def __setup__(cls):
        super(Location, cls).__setup__()
        cls._buttons.update({
            'on': {
                'invisible': Or(Eval('state') != 'off',
                    Eval('type') != 'production'),
                'depends': ['state', 'type']
            },
            'off': {
                'invisible': Or(Eval('state') != 'on',
                    Eval('type') != 'production'),
                'depends': ['state', 'type']
            }
        })

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)

        super(Location, cls).__register__(module_name)

        # Migration from 4.0
        table.not_null_action('state', action='remove')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('states', None)
        return super(Location, cls).copy(records, default=default)

    @classmethod
    def default_state(cls):
        return 'on'

    def get_state_icon(self, name=None):
        if self.state == 'off':
            return 'tryton-dialog-warning'
        return None

    @classmethod
    def get_state_at(cls, records, name=None):

        values = {r.id: r.state for r in records}
        at_date = Transaction().context.get('state_at_date', None)
        if not at_date:
            return values

        for record in records:
            for state in record.states:
                if state.date <= at_date:
                    values[record.id] = state.state
                    break
        return values

    @classmethod
    def search_state_at(cls, name, clause):
        State = Pool().get('stock.location.state')
        at_date = Transaction().context.get('state_at_date', None)
        if not at_date:
            return [('state', ) + tuple(clause[1:])]

        location_state = State.__table__()
        location_state2 = State.__table__()
        Operator = fields.SQL_OPERATORS[clause[1]]
        columns = [
            location_state.location,
            Max(location_state.id).as_('state_id'),
            Max(location_state.date).as_('date'),
        ]

        # Gets state of allowed max date
        query = location_state.select(
            *columns,
            where=(location_state.date <= at_date),
            group_by=[location_state.location])
        query = query.join(location_state2, condition=(
                query.state_id == location_state2.id)
            ).select(
                query.location,
                where=(Operator(location_state2.state, clause[2])))
        return [('id', 'in', query)]

    @classmethod
    @ModelView.button
    def off(cls, records):
        to_write = cls._get_records_to_change_state(records, 'on')
        if to_write:
            cls.write(list(to_write), {'state': 'off'})

    @classmethod
    @ModelView.button
    def on(cls, records):
        to_write = cls._get_records_to_change_state(records, 'off')
        if to_write:
            cls.write(list(to_write), {'state': 'on'})

    @classmethod
    def change_state(cls, new_state, record_ids=[]):
        if not record_ids:
            records = cls.search([('type', '=', 'production')])
        else:
            records = cls.browse(record_ids)
        func = getattr(cls, new_state)
        func(records)

    @classmethod
    def _get_records_to_change_state(cls, records, state):
        new_records = set(records)

        def get_childs(record):
            childs = set(c for c in record.childs)
            for c in record.childs:
                childs.update(get_childs(c))
            return childs

        for record in records:
            new_records.update((m for m in get_childs(record)
                               if m.state == state))
        return list(new_records)

    @classmethod
    def view_attributes(cls):
        return super(Location, cls).view_attributes() + [
            ('//page[@id="history"]', 'states', {
                'invisible': Eval('type') != 'production'})]

    @classmethod
    def get_last_states(cls, records, name):
        State = Pool().get('stock.location.state')
        location_state = State.__table__()
        cursor = Transaction().connection.cursor()

        # Gets last states
        query = location_state.select(
            RowNumber(window=Window(partition=[location_state.location])
                ).as_('counter'),
            location_state.location,
            location_state.id,
            order_by=(location_state.location, location_state.date.desc),
        )
        cursor.execute(*query.select(
            query.location,
            query.id,
            where=(query.counter <= cls._last_states_size))
        )
        values = cursor.fetchall()
        res = {r.id: [] for r in records}
        for location_id, state_ids in groupby(values, key=lambda v: v[0]):
            res[location_id] = [s[1] for s in state_ids]
        return res

    @classmethod
    def set_last_states(cls, records, name, value):
        with Transaction().set_context(create_history_state=False):
            cls.write(records, {
                    'states': value,
                })

    @classmethod
    def create(cls, vlist):
        records = super().create(vlist)
        states = {}
        for record in records:
            if record.state:
                states.setdefault(record.state, []).append(record)

        for state, stated_records in states.items():
            cls.create_history_state(stated_records, state)
        return cls.browse(records)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        states = {}
        for records, values in zip(actions, actions):
            if values.get('state', None) and \
                    Transaction().context.get('create_history_state', True):
                states.setdefault(values['state'], []).extend(records)
            args.extend((records, values))

        super().write(*args)

        for state, stated_records in states.items():
            cls.create_history_state(stated_records, state)

    @classmethod
    def create_history_state(cls, records, state):
        State = Pool().get('stock.location.state')

        if not state:
            return
        if not Transaction().context.get('create_history_state', True):
            # avoid create another record when editing history
            return

        values = []
        for record in records:
            values.append({
                'location': record.id,
                'state': state,
                'date': datetime.now()
            })
        State.create(values)

    @classmethod
    def set_current_state(cls, records):
        records = cls.browse(records)
        to_update = {}
        for record in records:
            if record.states and record.state != record.states[0].state:
                to_update.setdefault(record.states[0].state, []).append(record)
        if to_update:
            changes = []
            for k, v in to_update.items():
                changes.extend([v, {'state': k}])
            with Transaction().set_context(create_history_state=False):
                # do not use save due to context is lost
                cls.write(*changes)

    def get_state_time(self, from_date, to_date, state):
        # get first state outside from_date
        State = Pool().get('stock.location.state')

        first_state = State.search([
                ('location', '=', self.id),
                ('date', '<=', from_date)],
            order=[('date', 'DESC')],
            limit=1)

        history_states = State.search([
                ('location', '=', self.id),
                ('date', '>=', from_date),
                ('date', '<=', to_date)],
            order=[('date', 'ASC')])
        result = timedelta(0)
        if first_state:
            first_state, = first_state
        elif history_states:
            first_state = history_states[0]
        else:
            return None
        last_date = max(first_state.date, from_date)
        last_state = first_state.state
        for hstate in history_states:
            if last_state == state and last_state != hstate.state:
                result += (hstate.date - last_date)
            last_date = hstate.date
            last_state = hstate.state

        if last_state == state and last_date < to_date:
            result += (to_date - last_date)
        return result - timedelta(microseconds=result.microseconds)

    def get_state_time_on(self, from_date, to_date):
        return self.get_state_time(from_date, to_date, 'on')

    def get_state_time_off(self, from_date, to_date):
        return self.get_state_time(from_date, to_date, 'off')


class LocationState(ModelSQL, ModelView):
    """Stock location History State"""
    __name__ = 'stock.location.state'

    date = fields.DateTime('Date', required=True)
    location = fields.Many2One('stock.location', 'Location', select=True,
        required=True, ondelete='CASCADE')
    state = fields.Selection([
        ('on', 'On'),
        ('off', 'Off')], 'State', required=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('date', 'DESC'))
        t = cls.__table__()
        cls._sql_constraints += [
            ('date_location_uniq', Unique(t, t.date, t.location),
                'stock_location_history_state.'
                'msg_stock_location_state_date_location_uniq'),
        ]

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        Location = Pool().get('stock.location')
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        location = Location.__table__()

        super().__register__(module_name)

        cursor.execute(*table.select(table.id, limit=1))
        if table_h.table_exist('stock_location__history') and \
                not cursor.fetchone():
            history = Table('stock_location__history')
            history2 = Table('stock_location__history')
            query = history.join(location, condition=(
                        location.id == history.id)
                ).select(
                    history.id,
                    Max(Coalesce(history.write_date, history.create_date)
                        ).as_('date'),
                    where=(
                        (history.state != Null) &
                        (location.type == 'production')
                    ),
                    group_by=history.id
            )
            cursor.execute(*table.insert([
                    table.create_uid,
                    table.write_uid,
                    table.create_date,
                    table.write_date,
                    table.location,
                    table.date,
                    table.state
                ],
                history2.join(query, condition=(
                        (history2.id == query.id) &
                        (Coalesce(history2.write_date, history2.create_date
                            ) == query.date))
                    ).select(
                        history2.create_uid,
                        history2.write_uid,
                        history2.create_date,
                        history2.write_date,
                        history2.id,
                        query.date,
                        history2.state))
            )

    @classmethod
    def default_date(cls):
        return datetime.now()

    @fields.depends('location', '_parent_location.state')
    def on_change_location(self):
        mapping = {
            'on': 'off',
            'off': 'on'
        }
        if self.location and self.location.state:
            self.state = mapping[self.location.state]

    @classmethod
    def create(cls, vlist):
        Location = Pool().get('stock.location')

        records = super().create(vlist)
        locations = set([r.location for r in records])
        if locations:
            Location.set_current_state(locations)
        return records

    @classmethod
    def write(cls, *args):
        Location = Pool().get('stock.location')

        actions = iter(args)
        args = []
        locations = []
        for records, values in zip(actions, actions):
            locations.extend([r.location for r in records])
            args.extend((records, values))

        super().write(*args)

        if locations:
            locations = set(locations)
            Location.set_current_state(locations)

    @classmethod
    def delete(cls, records):
        Location = Pool().get('stock.location')
        locations = set([r.location for r in records])

        super().delete(records)

        if locations:
            Location.set_current_state(locations)


class Combined(metaclass=PoolMeta):
    __name__ = 'stock.location.combined'

    def _get_locations(self):
        value = super(Combined, self)._get_locations()
        return [l for l in value if l.state == 'on']
