# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    def get_production_time(self, name=None):
        result = None
        if self.production_location:
            result = self.production_location.get_state_time_on(
                self.start_date, self.end_date)
            if result is not None:
                return result
        return super().get_production_time(name)


class UnitLoadCombined(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    def get_production_time(self, name=None):
        if self.location_combined and self.production_locations:
            times = []
            for location in self.production_locations:
                time_ = location.get_state_time_on(
                    self.start_date, self.end_date)
                if time_ is not None:
                    times.append(time_)
            if times:
                return min(times)
        return super().get_production_time(name)
