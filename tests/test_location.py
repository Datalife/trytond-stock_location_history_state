# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class LocationStateHistoryTestCase(ModuleTestCase):
    """Test location state history module"""
    module = 'stock_location_history_state'

    @with_transaction()
    def test_change_state(self):
        """Change location state"""
        Location = Pool().get('stock.location')
        State = Pool().get('stock.location.state')
        transaction = Transaction()

        production, = Location.create([{
            'code': 'PL',
            'name': 'Production location',
            'type': 'production'
        }])
        self.assertEqual(production.state, 'on')
        transaction.commit()
        from_date = datetime.now().replace(microsecond=0
            ) - relativedelta(seconds=5)
        time.sleep(2)

        Location.off([production])
        self.assertEqual(production.state, 'off')

        transaction.commit()
        time.sleep(2)
        child, = Location.create([{
            'code': 'PL2',
            'name': 'Prod. location 2',
            'type': 'production',
            'parent': production.id,
            'state': 'off'
        }])
        time.sleep(1)
        Location.on([production])
        transaction.commit()
        self.assertTrue(production.state == 'on' and child.state == 'on')

        self.assertEqual(len(production.states), 3)
        values = {}
        for line in production.states:
            values.setdefault(line.state, 0)
            values[line.state] += 1
        self.assertEqual(values, {'on': 2, 'off': 1})

        self.assertEqual(production.state_at, 'on')

        at_date, = [h.date for h in production.states if h.state == 'off']
        at_date += relativedelta(seconds=0.5)

        with Transaction().set_context(state_at_date=at_date):
            other_loc, = Location.search([
                ('code', '=', 'PL'),
                ('state_at', '=', 'off')])
            self.assertEqual(production.id, other_loc.id)
            self.assertEqual(production.get_state_at(
                [production])[production.id], 'off')
            self.assertEqual(Location.search([
                ('code', '=', 'PL'),
                ('state_at', '=', 'on')]) or [], [])

        # check time in on state
        self.assertEqual(production.get_state_time_on(from_date,
            from_date + relativedelta(seconds=9)), timedelta(seconds=2))
        self.assertEqual(production.get_state_time_on(
            from_date + relativedelta(seconds=8),
            from_date + relativedelta(seconds=12)), timedelta(seconds=2))
        self.assertEqual(production.get_state_time_on(
            from_date + relativedelta(seconds=6),
            from_date + relativedelta(seconds=7)), timedelta(seconds=1))
        self.assertEqual(
            production.get_state_time_on(
                from_date + relativedelta(seconds=5),
                from_date + relativedelta(seconds=12)
            ) +
            production.get_state_time_off(
                from_date + relativedelta(seconds=5),
                from_date + relativedelta(seconds=12)
            ), timedelta(seconds=7))

        # check update current state
        self.assertEqual(len(production.states), 3)
        State.delete([production.states[0]])
        transaction.commit()
        self.assertEqual(production.state, 'off')
        self.assertEqual(len(production.states), 2)
        Location.write([production], {
            'last_states': [('delete', [production.states[-1]])]
            })
        transaction.commit()
        production = Location(production.id)
        self.assertEqual(production.state, 'off')
        self.assertEqual(len(production.states), 1)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        LocationStateHistoryTestCase))
    return suite
